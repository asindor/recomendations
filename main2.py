import json
import pickle
from collections import Counter
from numpy.linalg import norm

import nltk
from sklearn.decomposition import TruncatedSVD
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
import numpy as np
import pandas as pd
from dump_generator import DumpGenerator
import time

data_path = r'C:\Users\BlueFox\Desktop\Recommendations'
dumps_folder = 's-sobstven'
dump_generator = DumpGenerator(data_path=data_path, dumps_folder=dumps_folder)

user_id = 1616609246848239640
with open(r'{}\users\{}.json'.format(data_path, user_id), 'r', encoding='utf-8') as f:
    user_history = json.load(f)


def save_tfidf_docs(tfidf, name):
    tfidf.to_pickle(r'{}\{}\tfidf-{}.dump'.format(data_path, dumps_folder, name))


def load_tfidf_docs(name):
    return pd.read_pickle(r'{}\{}\tfidf-{}.dump'.format(data_path, dumps_folder, name))


def save_vocabulary_tfidf_docs(vocabulary, tfidf_docs, tags_counter, name):
    with open(r'{}\{}\tfidf-vocabulary-{}.dump'.format(data_path, dumps_folder, name), 'wb') as dump_file:
        pickle.dump(vocabulary, dump_file, protocol=4)
        with open(r'{}\{}\tfidf-tags_count-{}.dump'.format(data_path, dumps_folder, name), 'wb') as dump_file:
            pickle.dump(tags_counter, dump_file, protocol=4)
    tfidf_docs.to_pickle(r'{}\{}\tfidf-docs-{}.dump'.format(data_path, dumps_folder, name))


def load_vocabulary_and_tfidf_docs(name):
    with open(r'{}\{}\tfidf-vocabulary-{}.dump'.format(data_path, dumps_folder, name), 'rb') as dump_file:
        vocabulary = pickle.load(dump_file)
    with open(r'{}\{}\tfidf-tags_count-{}.dump'.format(data_path, dumps_folder, name), 'rb') as dump_file:
        tags_counter = pickle.load(dump_file)
    tfidf_docs = pd.read_pickle(r'{}\{}\tfidf-docs-{}.dump'.format(data_path, dumps_folder, name))
    return vocabulary, tfidf_docs, tags_counter


def calculate_tfidf():
    file_numbers_range = range(1, 11)
    materials_slice = 10000
    corpus, tags = dump_generator.load_docs(file_numbers_range, materials_slice)
    tfidf = TfidfVectorizer(
        encoding="utf-8",
        # stop_words=stopwords.words('russian'),
        lowercase=False,
        token_pattern=r'.*',
        tokenizer=dump_generator.tokenize_doc,
        # preprocessor=dump_generator.tokenize_doc,
        # min_df=1,
        min_df=20,
        # max_df=0.1,  # Слова, встречающиеся в 30% документов просто игнорируем
        # ngram_range=(1, 2),
    )
    # vectorizer = CountVectorizer(
    #     encoding="utf-8",
    #     stop_words=stopwords.words('russian'),
    #     lowercase=True,
    #     tokenizer=dump_generator.tokenize_doc
    # )
    # .toarray() делать важно, иначе отвалится svd
    tfidf_docs_t = tfidf.fit_transform(corpus).toarray()
    tfidf_docs = pd.DataFrame(tfidf_docs_t)
    # tfidf_docs - tfidf_docs.mean()
    tags_counter = Counter([tag for sublist in tags for tag in sublist])
    # Количество тегов, которые встречаются более 5 раз
    # [t for t in tags_counter.values() if t > 5]
    print("Vocabulary size {}".format(len(tfidf.vocabulary_)))
    print("Different tags size {}".format(len(tags_counter)))
    # Будем хранить на диске обе.
    return tfidf, tfidf_docs, tags_counter


def build_truncated_svd(tfidf_docs, n_components):
    svd = TruncatedSVD(n_components=n_components, n_iter=20)
    svd_topic_vectors = svd.fit_transform(tfidf_docs.values)
    return svd, svd_topic_vectors


def save_truncated_svd(svd, svd_topic_vectors, name):
    with open(r'{}\{}\svd\{}-svd_topic_vectors.dump'.format(data_path, dumps_folder, name), 'wb') as dump_file:
        pickle.dump(svd_topic_vectors, dump_file, protocol=4)
    with open(r'{}\{}\svd\{}-svd_components.dump'.format(data_path, dumps_folder, name), 'wb') as dump_file:
        pickle.dump(svd.components_, dump_file, protocol=4)

#
# def load_truncated_svd(name):
#     with open(r'{}\{}\svd\{}-svd_topic_vectors.dump'.format(data_path, dumps_folder, name), 'rb') as dump_file:
#         svd_topic_vectors =  pickle.load(dump_file)
#     with open(r'{}\{}\svd\{}-svd_topic_vectors.dump'.format(data_path, dumps_folder, name), 'rb') as dump_file:
#         return pickle.load(dump_file)


def load_svd_topic_vectors(name):
    with open(r'{}\{}\svd\{}-svd_topic_vectors.dump'.format(data_path, dumps_folder, name), 'rb') as dump_file:
        return pickle.load(dump_file)


def load_svd_components(name):
    with open(r'{}\{}\svd\{}-svd_components.dump'.format(data_path, dumps_folder, name), 'rb') as dump_file:
        return pickle.load(dump_file)


def document_features(doc_content, vocabulary):
    # Можно виртуально рассматривать это как генерацию фич
    terms = dump_generator.tokenize_doc(doc_content)
    features = [term for term in terms if term in vocabulary]
    features_matrix = np.zeros(shape=(len(vocabulary), 1))
    for feature in features:
        features_matrix[vocabulary[feature]][0] = 1
    return features, features_matrix


def load_doc_content():
    file_numbers_range = range(1, 2)
    materials_slice = 1
    return dump_generator.load_docs(file_numbers_range, materials_slice)


def test(vocabulary, svd_components):
    doc, tags = load_doc_content()
    features, features_matrix = document_features(doc[0], vocabulary)
    topics = svd_components.dot(features_matrix)
    return topics


def calculate_doc_features(text, vocabulary, svd_components):
    features, features_matrix = document_features(text, vocabulary)
    topics = svd_components.dot(features_matrix)
    return topics


def top_n_valuable_feature_names(vocabulary, svd_components, topic_number, n):
    inverse_dictionary = {v: k for k, v in vocabulary.items()}
    top_features_indexes = sorted(range(len(svd_components[topic_number])), key=lambda i: svd_components[topic_number][i], reverse=True)[:n]
    feature_names = [inverse_dictionary[feature_index] for feature_index in top_features_indexes]
    feature_values = [svd_components[topic_number][feature_index] for feature_index in top_features_indexes]
    print("For topic with index {} top features are {} with values {}".format(topic_number, feature_names, feature_values))
    return feature_names, feature_values


def top_n_document_features(document_topics, vocabulary, svd_components, n):
    top_document_topics = sorted(range(len(document_topics)), key=lambda i: document_topics[i], reverse=True)[:n]
    topic_values = [document_topics[topic_number] for topic_number in top_document_topics]
    print("Topic values {}".format(topic_values))
    for i in top_document_topics:
        top_n_valuable_feature_names(vocabulary, svd_components, i, 10)
    print("")


def cosine_similarity(a, b):
    return a.dot(b) / (norm(a) * norm(b))

# 1. Строим tf-idf
# 2. Отделяем словарь, находим словарь, сортируем
# 3. Вычисляем TruncatedSVD
# 4. Модель, - это словарь и соответствующая ему матрица (топик, фича)
# Как это использовать? Каждый материал теперь можно разложить на топики.
# Сколько топиков выбрать? 8000 это чересчур даже для всех 380_000 материалов.


start_time = time.time()
print("--- %s seconds ---" % (time.time() - start_time))
# tfidf, tfidf_docs, tags_counter = calculate_tfidf()
print("--- %s seconds ---" % (time.time() - start_time))
# vocabulary = sorted(tfidf.vocabulary_)
# save_vocabulary_tfidf_docs(tfidf.vocabulary_, tfidf_docs, tags_counter, "100000_mindf_20_max_df_all_sobstv_cleared")
vocabulary, tfidf_docs, tags_counter = load_vocabulary_and_tfidf_docs("100000_mindf_20_max_df_all_sobstv_cleared")
for t in sorted(vocabulary):
    print("['{}', ], ".format(t))
print("")

# print("--- %s seconds ---" % (time.time() - start_time))
# tfidf_docs = load_tfidf("1000_mindf_10")
# svd, svd_topic_vectors = build_truncated_svd(tfidf_docs, 1000)
# save_truncated_svd(svd, svd_topic_vectors, "100000_mindf_20_max_df_all_sobstv_cleared_1000_topics")
# svd_components = load_svd_components("100000_mindf_20_max_df_all_sobstv_cleared_3000_topics")
# svd_topic_vectors = load_svd_topic_vectors("100000_mindf_20_max_df_all_sobstv_cleared_3000_topics")
# print("--- %s seconds ---" % (time.time() - start_time))
# svd_components = load_svd_components("10000_mindf_10")

# f = []
# for d in user_history:
#     document_topics = calculate_doc_features(d, vocabulary, svd_components).flatten()
#     f.append(document_topics)
#     top_n_document_features(document_topics, vocabulary, svd_components, 15)
# print("")
# for i in range(0, svd_topic_vectors.shape[1]):
#     top_n_valuable_feature_names(vocabulary, svd_components, i, 10)
# topic = test(vocabulary, svd_components)
# print("--- %s seconds ---" % (time.time() - start_time))
# for i in range(0, 10000):
#     cosine = cosine_similarity(svd_topic_vectors[i], topic)
#     print("Cosine similarity {}".format(cosine))

# u,s, t = load_svd("50000_mindf_10")
# print("--- %s seconds ---" % (time.time() - start_time))
# u, s, vt = build_simple_svd(tfidf_docs)
# save_svd(u, s, vt, "100000_mindf_10")
# u, s, vt = load_svd("1000_mindf_10")
# print("--- %s seconds ---" % (time.time() - start_time))

# 1000 documents 40 sec (min_df=1), 17 sec(min_df=10)
# 10000
# documents 1813 sec (30 min)(min_df=1), 336 sec (153 from which is not calculating svd, min_df=10) словарь 7758, тегов 2768
# 50000
# documents 1961 sec min_df=10 (calculating tfidf sec) tags - 5000, dictionary - 19000
# documents 848 sec min_df=20 (calculating tfidf sec) tags - 5028 , dictionary - 12983
# 100_000
# min_df = 10 vocabulary 27824
# (6139 разных тегов, но лишь 3200 встречаются более 5 раз!, лишь 2625 более 10 раз)
# Реалистичное количество топиков, - 3200.
# min_df = 30, 1756 sec, dictionary -15483
# 150_000
# vocabulary 33178
# 200_000
# dictionary 16898 mindf 50 took 3501 seconds
# 380_000
# 8 разных тегов, но лишь 5513 встречаются болеее 5 раз, лишь 4500 более 10 раз, лишь 3300 более 20 раз
# Реалистичное количество топиков, - 4000-5000 тысяч

# min_df = 10, 50_000 документов, словарь -19186
# Заметки, - я пробовал svd  даже с full_matrices=False слишком долго.
# При 300_000 документах и размере словаря в 20_000, потребуется 6гб памяти.
# float64 - 1 байт. 1Гб = 10^9 ,байт

# Главное уметь по имеющейся истории просмотров разобраться какие материаоы близки к просмотренным
# Пробуем работать с TruncatedSVD, понятно как с этим работать!


def build_simple_svd(tfidf_docs):
    u, s, vt = np.linalg.svd(tfidf_docs, full_matrices=False)
    return u, s, vt


def save_svd(u, s, v, name):
    with open(r'{}\{}\svd\{}-u.dump'.format(data_path, dumps_folder, name), 'wb') as dump_file:
        pickle.dump(u, dump_file, protocol=4)
    with open(r'{}\{}\svd\{}-s.dump'.format(data_path, dumps_folder, name), 'wb') as dump_file:
        pickle.dump(s, dump_file, protocol=4)
    with open(r'{}\{}\svd\{}-v.dump'.format(data_path, dumps_folder, name), 'wb') as dump_file:
        pickle.dump(v, dump_file, protocol=4)


def load_svd(name):
    with open(r'{}\{}\svd\{}-u.dump'.format(data_path, dumps_folder, name), 'rb') as dump_file:
        u = pickle.load(dump_file)
    with open(r'{}\{}\svd\{}-s.dump'.format(data_path, dumps_folder, name), 'rb') as dump_file:
        s = pickle.load(dump_file)
    with open(r'{}\{}\svd\{}-v.dump'.format(data_path, dumps_folder, name), 'rb') as dump_file:
        v = pickle.load(dump_file)
    return u, s, v
